/*
Utilized code from John's presentation documentation,
received assistance from various classmates regarding design
organization.

Wored in conjunction with Maya Rouillard and Sam Amundson.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
//#include <pthread.h>
#include <ncurses.h>

#define SERVER "10.115.20.250"
#define PORT 49154
#define BUFSIZE 1024
pthread_t threadOne, threadTwo;

int is_done = 0;
int fd = 0;
int len, yMax, xMax;
int origbuffer;
int charbuffer;

//func prototyping
int connect2v4stream (char *, int);
int sendout(int, char *);
void recvandprint (int, char *);
void sendthreader(void *);
void recthreader(void *);

int connect2v4stream (char * srv, int port) {
	int ret,sockd;
	struct sockaddr_in sin;
	//check for solid connection - if error code rec'd from
	//server, exit
	if ( (sockd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		printf("ERROR: trouble creating socket. errno = %d\n", errno);
		exit(errno);
	}
	if ( (ret = inet_pton(AF_INET, SERVER, &sin.sin_addr)) <= 0 ){
		printf("ERROR: trouble converting using inet_pton. \
				return value = %d, errno = %d\n", ret, errno);
		exit(errno);
	}
	sin.sin_family = AF_INET;
	sin.sin_port = htons(PORT);

	if ( (connect(sockd, (struct sockaddr *) &sin, sizeof(sin))) == -1 ){
		printf("ERROR: trouble connectig to server. errno = %d\n",errno);
		exit(errno);
	}
	return sockd;
}
int sendout( int fd, char *msg ) {

	int ret;
	ret=send(fd, msg, strlen(msg), 0);
	if ( ret == -1){
		printf("ERROR: trouble sending. errno = %d\n", errno);
		exit(errno);
	}
	return strlen(msg);
}

/*
Receives and prints strings froma socket to
stdout until there is no more input from the socket
*/
void recvandprint (int fd, char *buff ){

	int ret;

	for(;;) {
		buff = malloc(BUFSIZE+1);
		ret = recv(fd,buff,BUFSIZE,0);
		if(ret==-1){
			if(errno == EAGAIN){
				break;
			} else {
				printf("ERROR: error receiving. errno = %d\n", errno);
				exit(errno);
			}
		} else if (ret == 0) {
			exit(0);
		} else {
			buff[ret] = 0;
			printf("%s",buff);
		}
		free(buff);
	}
}

void sendthreader(void *thing) {

	int len = BUFSIZE;
	char *buffer = malloc(len+1);
	char *origbuffer = buffer;

	if (getline(&buffer, (size_t *) &len,stdin) > 1 ) {
			sendout(fd, buffer);
	is_done = (strcmp (buffer, "quit\n") == 0);
	free(origbuffer);
	}
}

void recthreader(void *thing) {

	char *buffer = malloc(len+1);

	while( ! is_done ) {
		recvandprint(fd,buffer);
	}
}
/*
void windowmaker(void *thing){


	initscr();
	// noecho();
	getmaxyx(stdscr, yMax, xMax);

	// create new windows
	WINDOW * inputwin = newwin(4, xMax-20, yMax-40, 5);
	box(inputwin, 0, 0);
	refresh();
	wrefresh(inputwin);

	WINDOW * outputwin = newwin(4, xMax-20, yMax-20, 5);
	box(outputwin, 0, 0);
	cbreak();
	refresh();
	wrefresh(outputwin);

	//Allows scrolling and keypad usage
	keypad(outputwin, true);
	keypad(inputwin, true);
	scrollok(outputwin, true);
	scrollok(inputwin, true);
}
*/





int main(int argc, char * argv[]) {

	int fd, len;
	char *name, *buffer, *origbuffer;
	struct timeval timev;
	//char msg[50];
	// get screen size
//	int yMax, xMax;

	fd = connect2v4stream( SERVER, PORT);
	//sendthreader();
	//ecthreader();
	//windowmaker();

	timev.tv_sec = 0;
	timev.tv_usec = 1000 * 500;
	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));

	if(argc <2) {
		printf("User's chat <screenname>\n");
		exit(1);
	}
	name = argv[1];
	len = strlen(name);
	name[len] = '\n';
	name[len+1] = '\0';
	sendout(fd, name);

	int is_done = 0;
	while (! is_done) {
		recvandprint(fd, buffer);
	}

	pthread_create(&threadOne, NULL, sendthreader, NULL);
	pthread_create(&threadTwo, NULL, recthreader, NULL);
	pthread_join(threadOne, NULL);
	pthread_join(threadTwo, NULL);
}


	/*
	//init thread setup

	//send func

	//receive func

	}*/


	//endwin();
